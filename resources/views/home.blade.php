@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h1 class="text-xl mb-4">Dashboard</h1>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert--success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="alert alert--info">
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
