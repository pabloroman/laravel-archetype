<div class="alert alert--info" role="alert">
  <div class="flex">
    <div class="mr-2"><i class="fas fa-info-circle"></i></div>
    <div>
      <p class="font-bold">Our privacy policy has changed</p>
      <p class="text-sm">Make sure you know how these changes affect you.</p>
    </div>
  </div>
</div>