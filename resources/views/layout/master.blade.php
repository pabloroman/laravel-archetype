<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <title>{{ config('app.name') }}</title>
    </head>
    <body class="bg-gray-200">

        @include('partials.navigation')

        <div class="container mx-auto">
        <div class="flex flex-wrap mb-4">
            <div class="w-full">
                @yield('content')
            </div>
        </div>
        </div>
    </body>
</html>
