@extends('layout.master')

@section('content')

    <div class="px-3 lg:px-6">
    @include('partials.alert')

    @include('partials.card')

    @include('partials.form')
    </div>

@stop