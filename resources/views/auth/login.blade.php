@extends('layout.master')

@section('content')
<div class="shadow bg-white p-4 md:p-8">
    <h1 class="text-xl mb-4">{{ __('Login') }}</h1>

    <div class="">
        <form method="POST" action="{{ route('auth.login') }}">
            @csrf

            <div class="form-block">
                <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                <div class="">
                    <input id="email" type="email" class="input @error('email') input--error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-block">
                <label for="password" class="label">{{ __('Password') }}</label>

                <div class="">
                    <input id="password" type="password" class="input @error('password') input--error @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-block">
                <div class="">
                    <div class="mb-4">
                        <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="">
                    <button class="button button--main" type="submit">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
