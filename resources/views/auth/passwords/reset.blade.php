@extends('layout.master')

@section('content')
<div class="shadow bg-white p-4 md:p-8">
    <h1 class="text-xl mb-4">{{ __('Reset Password') }}</h1>

    <div class="card-body">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-block">
                <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="input @error('email') input--error @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="feedback--error" role="alert">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-block">
                <label for="password" class="label">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="input @error('password') input--error @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="feedback--error" role="alert">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-block">
                <label for="password-confirm" class="label">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="input" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="form-block mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="button button--main">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
