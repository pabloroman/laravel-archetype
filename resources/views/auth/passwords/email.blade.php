@extends('layout.master')

@section('content')
<div class="shadow bg-white p-4 md:p-8">
    <h1 class="text-xl mb-4">{{ __('Reset Password') }}</h1>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert--success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-block">
                <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="input @error('email') input--error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="feedback--error" role="alert">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-block mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="button button--main">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
